
class Game {

  private _canvas: HTMLCanvasElement;
  private _engine: BABYLON.Engine;
  public static _scene: BABYLON.Scene;
  private _camera: BABYLON.FreeCamera;
  private _light: BABYLON.Light;
	private _advancedTexture : BABYLON.GUI.AdvancedDynamicTexture;
	private _panel : BABYLON.GUI.StackPanel;
  private _loader : BABYLON.AssetsManager;
  private _animPanel : AnimatedPanel;
  private _stickerController : StickerController;

  constructor(canvasElement: string) {
    // Create canvas and engine
    this._canvas = <HTMLCanvasElement>document.getElementById(canvasElement);
    this._engine = new BABYLON.Engine(this._canvas, true);
  }

  createScene(): void {
    
    Game._scene = new BABYLON.Scene(this._engine);
    this._stickerController = new StickerController(Game._scene);
    this._animPanel = new AnimatedPanel();
    this._loader = new BABYLON.AssetsManager(Game._scene);
		this._advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
    this._camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(-13.75, -0.35,  0.5),Game._scene);
    this._camera.setTarget(BABYLON.Vector3.Zero());
    this._camera.attachControl(this._canvas, false);
    this._light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), Game._scene);
    

    //przyklad ui
    this._advancedTexture.addControl(this._animPanel.stackPanel);
    
    //cuby render textura (przyklad jak bedzie dzialal sticker)
    //this._stickerController.CreateLatexableCube();
    //this._stickerController.CreateSticker();
    
  }



  animate(): void {
    this._engine.runRenderLoop(() => {
      Game._scene.render();
    });
    window.addEventListener('resize', () => {
      this._engine.resize();
    });
  }
}

window.addEventListener('DOMContentLoaded', () => {

  let game = new Game('renderCanvas');
  game.createScene();
  game.animate();
});


