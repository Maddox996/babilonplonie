class Animations {

    public static GetWidthScaleAnimation(beginFrameValue : number, endFrame : number, lastFrameValue : number) : BABYLON.Animation
    {
      var anim = new BABYLON.Animation("myAnimation", "width", 60, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE);
      anim.setKeys(this.GetKeys(beginFrameValue, endFrame, lastFrameValue));
      return anim;
    }
  
    public static GetHeightScaleAnimation(beginFrameValue : number, endFrame : number, lastFrameValue : number) : BABYLON.Animation
    {
      var anim = new BABYLON.Animation("myAnimation", "height", 60, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE);
      anim.setKeys(this.GetKeys(beginFrameValue, endFrame, lastFrameValue));
      return anim;
    }
  
    public static GetKeys(beginFrameValue : number, endFrame : number, lastFrameValue : number) : any
    { 
      var keys = [];
      
      keys.push({
        frame: 0,
        value: beginFrameValue
      });
  
      keys.push({
      frame: endFrame,
      value: lastFrameValue
      });
  
      return keys;
    }
  
}