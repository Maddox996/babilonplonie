var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class Game {
    constructor(canvasElement) {
        // Create canvas and engine
        this._canvas = document.getElementById(canvasElement);
        this._engine = new BABYLON.Engine(this._canvas, true);
    }
    createScene() {
        Game._scene = new BABYLON.Scene(this._engine);
        this._stickerController = new StickerController(Game._scene);
        this._animPanel = new AnimatedPanel();
        this._loader = new BABYLON.AssetsManager(Game._scene);
        this._advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
        this._camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(-13.75, -0.35, 0.5), Game._scene);
        this._camera.setTarget(BABYLON.Vector3.Zero());
        this._camera.attachControl(this._canvas, false);
        this._light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), Game._scene);
        //przyklad ui
        this._advancedTexture.addControl(this._animPanel.stackPanel);
        //cuby render textura (przyklad jak bedzie dzialal sticker)
        //this._stickerController.CreateLatexableCube();
        //this._stickerController.CreateSticker();
    }
    animate() {
        this._engine.runRenderLoop(() => {
            Game._scene.render();
        });
        window.addEventListener('resize', () => {
            this._engine.resize();
        });
    }
}
window.addEventListener('DOMContentLoaded', () => {
    let game = new Game('renderCanvas');
    game.createScene();
    game.animate();
});
class AnimatedPanel {
    constructor() {
        this.stackPanel = new BABYLON.GUI.StackPanel();
        this.stackPanel.isVertical = false;
        this._buttonFactory = new ButtonFactory(); // !
        this.isShown = false;
        this.buttons = new Array();
        this.TestInit(); // !
    }
    AddButton(height, width, callback) {
        var control = new BABYLON.GUI.Container("container");
        control.width = width.toString() + "px";
        control.height = height.toString() + "px";
        var button = this._buttonFactory.CreateImageButton(0, 0); // !
        var animations = [];
        animations.push(Animations.GetWidthScaleAnimation(0, 60, width)); // !
        animations.push(Animations.GetHeightScaleAnimation(0, 60, height)); // !
        button['animations'] = animations;
        control.addControl(button);
        this.stackPanel.addControl(control);
        button.onPointerDownObservable.add(callback, 1, true, this);
        this.buttons.push(button);
        return button;
    }
    TestInit() {
        for (var i = 0; i < 5; i++) {
            var but = this.AddButton(150, 150, this.HidePanel);
        }
        this.ShowPanel();
    }
    Delay(milliseconds) {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve();
            }, milliseconds);
        });
    }
    ShowPanel() {
        return __awaiter(this, void 0, void 0, function* () {
            var panel = this;
            for (var j = 0; j < panel.buttons.length; j++) {
                yield this.Delay(50).then(function () {
                    panel.FireAnimations(panel.buttons, j, 0, 60);
                });
            }
        });
    }
    HidePanel(a, b) {
        return __awaiter(this, void 0, void 0, function* () {
            var panel = this;
            for (var j = 0; j < panel.buttons.length; j++) {
                yield this.Delay(50).then(function () {
                    panel.FireAnimations(panel.buttons, j, 60, 0);
                });
            }
        });
    }
    FireAnimations(buttons, i, startFrame, endFrame) {
        Game._scene.beginAnimation(buttons[i], startFrame, endFrame, false, 5.0);
    }
}
class ButtonFactory {
    CreateImageButton(width, height) {
        var button = BABYLON.GUI.Button.CreateImageOnlyButton("button", "Decals.png");
        button.width = width.toString() + "px";
        button.height = height.toString() + "px";
        button.paddingLeft = 20; //!!!!
        return button;
    }
}
class Animations {
    static GetWidthScaleAnimation(beginFrameValue, endFrame, lastFrameValue) {
        var anim = new BABYLON.Animation("myAnimation", "width", 60, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE);
        anim.setKeys(this.GetKeys(beginFrameValue, endFrame, lastFrameValue));
        return anim;
    }
    static GetHeightScaleAnimation(beginFrameValue, endFrame, lastFrameValue) {
        var anim = new BABYLON.Animation("myAnimation", "height", 60, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE);
        anim.setKeys(this.GetKeys(beginFrameValue, endFrame, lastFrameValue));
        return anim;
    }
    static GetKeys(beginFrameValue, endFrame, lastFrameValue) {
        var keys = [];
        keys.push({
            frame: 0,
            value: beginFrameValue
        });
        keys.push({
            frame: endFrame,
            value: lastFrameValue
        });
        return keys;
    }
}
class StickerController {
    constructor(scene) {
        this.scene = scene;
        this.camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 55, -10), Game._scene, false);
        this.renderTexture = new BABYLON.RenderTargetTexture("render", 512, this.scene, true);
        this.renderTexture.coordinatesIndex = 0;
        this.renderTexture.activeCamera = this.camera;
        this.stickers = [];
        this.scene.customRenderTargets.push(this.renderTexture);
    }
    CreateSticker() {
        var sticker = new Sticker(this.scene);
        this.stickers.push(sticker);
        this.renderTexture.renderList.push(sticker.stickerMesh);
        StickerController.currentSticker = sticker;
    }
    CreateLatexableCube() {
        var cube2 = BABYLON.Mesh.CreateBox("x", 3, this.scene);
        var whiteTex = new BABYLON.Texture("/textures/white.jpg", this.scene);
        whiteTex.coordinatesIndex = 1;
        var material2 = new BABYLON.StandardMaterial("mat2", this.scene);
        material2.bumpTexture = this.renderTexture;
        material2.diffuseTexture = whiteTex;
        cube2.material = material2;
    }
}
class Sticker {
    constructor(scene) {
        this.scene = scene;
        this.stickerMesh = BABYLON.Mesh.CreatePlane("plane", 5, this.scene);
        this.stickerMesh.position.y += 55.0;
        var material0 = new BABYLON.StandardMaterial("mat0", this.scene);
        material0.diffuseColor = new BABYLON.Color3(1, 0, 0);
        material0.bumpTexture = new BABYLON.Texture("/textures/normalMap.jpg", this.scene);
        this.stickerMesh.material = material0;
        var x = 0;
        this.scene.registerAfterRender(function () {
            x += 0.05;
            StickerController.currentSticker.stickerMesh.position.x = Math.sin(x);
        });
    }
}
class QuadMesh {
    static Create(size) {
        //przykladowe tworzenie mesha
        //https://babylonjsguide.github.io/advanced/Custom
        var mesh = new BABYLON.Mesh("custom", Game._scene);
        var vertexData = new BABYLON.VertexData();
        var positions = [0, 0, 0, 0, -size.y, 0, size.x, -size.y, 0, size.x, 0, 0];
        var tris = [0, 1, 2, 2, 3, 0];
        var normals = [];
        var uv = [0, 1, 0, 0, 1, 0, 1, 1];
        var uv2 = [0, 1, 0, 0, 1, 0, 1, 1];
        BABYLON.VertexData.ComputeNormals(positions, tris, normals);
        vertexData.positions = positions;
        vertexData.indices = tris;
        vertexData.normals = normals;
        vertexData.uvs = uv;
        vertexData.uvs2 = uv;
        vertexData.applyToMesh(mesh);
        return mesh;
    }
}
class Skull {
    constructor(scene) {
        this.scene = scene;
        this.loader = new BABYLON.AssetsManager(scene);
        this._vertexData = new BABYLON.VertexData();
    }
    Create() {
        var meshTask = this.loader.addMeshTask("skull task", "", "/textures/", "skull.babylon");
        this.loader.load();
        meshTask.onSuccess = function (task) {
            // var renderTexture = new BABYLON.RenderTargetTexture("render", 512, this.scene, true);
            // renderTexture.renderList.push(plane);
            // renderTexture.coordinatesIndex = 0;
            // renderTexture.activeCamera = this._camera;
            // var whiteTex = new BABYLON.Texture("/textures/white.jpg", this.scene);
            // whiteTex.coordinatesIndex = 1;
            // var material2 = new BABYLON.StandardMaterial("mat2", this.scene);
            // material2.bumpTexture = renderTexture;
            // material2.diffuseTexture = whiteTex;
            // this.cube = task.loadedMeshes[0];
            // this._vertexData = BABYLON.VertexData.ExtractFromMesh(this.cube);
            // this._vertexData.uvs2 = [].concat(this._vertexData.uvs);
            // this._vertexData.applyToMesh(this.cube);
            // this.cube.material = material2;
        };
        // this.scene.customRenderTargets.push(renderTexture);
    }
}
