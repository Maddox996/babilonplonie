var Animations = /** @class */ (function () {
    function Animations() {
    }
    Animations.GetWidthScaleAnimation = function (beginFrameValue, endFrame, lastFrameValue) {
        var anim = new BABYLON.Animation("myAnimation", "width", 60, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE);
        anim.setKeys(this.GetKeys(beginFrameValue, endFrame, lastFrameValue));
        return anim;
    };
    Animations.GetHeightScaleAnimation = function (beginFrameValue, endFrame, lastFrameValue) {
        var anim = new BABYLON.Animation("myAnimation", "height", 60, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE);
        anim.setKeys(this.GetKeys(beginFrameValue, endFrame, lastFrameValue));
        return anim;
    };
    Animations.GetKeys = function (beginFrameValue, endFrame, lastFrameValue) {
        var keys = [];
        keys.push({
            frame: 0,
            value: beginFrameValue
        });
        keys.push({
            frame: endFrame,
            value: lastFrameValue
        });
        return keys;
    };
    return Animations;
}());
