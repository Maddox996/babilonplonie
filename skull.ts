class Skull{


    scene : BABYLON.Scene;
    loader : BABYLON.AssetsManager;
    _camera : BABYLON.FreeCamera;
    _vertexData : BABYLON.VertexData;
    cube : BABYLON.Mesh;
    constructor(scene : BABYLON.Scene)
    {
        this.scene = scene;
        this.loader = new BABYLON.AssetsManager(scene);    
        this._vertexData = new BABYLON.VertexData();
    }

    public Create() : void{

        var meshTask = this.loader.addMeshTask("skull task", "", "/textures/", "skull.babylon");
        this.loader.load();
        
        meshTask.onSuccess = function(task){

            // var renderTexture = new BABYLON.RenderTargetTexture("render", 512, this.scene, true);
            // renderTexture.renderList.push(plane);
            // renderTexture.coordinatesIndex = 0;
            // renderTexture.activeCamera = this._camera;
            
            // var whiteTex = new BABYLON.Texture("/textures/white.jpg", this.scene);
            // whiteTex.coordinatesIndex = 1;

            // var material2 = new BABYLON.StandardMaterial("mat2", this.scene);
            // material2.bumpTexture = renderTexture;
            // material2.diffuseTexture = whiteTex;
            
            // this.cube = task.loadedMeshes[0];
            // this._vertexData = BABYLON.VertexData.ExtractFromMesh(this.cube);
            // this._vertexData.uvs2 = [].concat(this._vertexData.uvs);
            // this._vertexData.applyToMesh(this.cube);
            // this.cube.material = material2;

        }

        // this.scene.customRenderTargets.push(renderTexture);

    }
}