class QuadMesh{


    static Create(size : BABYLON.Vector2) : BABYLON.Mesh {

        //przykladowe tworzenie mesha
        //https://babylonjsguide.github.io/advanced/Custom
        var mesh = new BABYLON.Mesh("custom", Game._scene);
        var vertexData = new BABYLON.VertexData();

        var positions = [0, 0, 0,  0, -size.y, 0,  size.x, -size.y, 0,  size.x, 0, 0];
        var tris = [0, 1, 2, 2, 3, 0];
        var normals = [];
        var uv = [0,1,  0,0,  1,0,  1,1];
        var uv2 = [0,1, 0,0, 1,0, 1,1];
        BABYLON.VertexData.ComputeNormals(positions, tris, normals);
        
        vertexData.positions = positions;
        vertexData.indices = tris;
        vertexData.normals = normals;
        vertexData.uvs = uv;
        vertexData.uvs2 = uv; 
        vertexData.applyToMesh(mesh);

        return mesh;
    }
}