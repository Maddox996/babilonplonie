var AnimatedPanel = /** @class */ (function () {
    function AnimatedPanel() {
        this._panel = new BABYLON.GUI.StackPanel();
        this._panel.isVertical = false;
        this._buttonFactory = new ButtonFactory();
    }
    AnimatedPanel.prototype.AddButton = function () {
        var button = this._buttonFactory.CreateImageButton(1, 1);
        var animations = [];
        animations.push(Animations.GetWidthScaleAnimation(0, 60, 150));
        animations.push(Animations.GetHeightScaleAnimation(0, 60, 150));
        button['animations'] = animations;
        this._panel.addControl(button);
        return button;
    };
    AnimatedPanel.prototype.Init = function () {
        this.buttons = new Array();
        for (var i = 0; i < 5; i++) {
            var but = this.AddButton();
            but.onPointerClickObservable.add(this.HidePanel);
            this.buttons.push(but);
            console.log(this.buttons[i]);
        }
        this.ShowPanel();
    };
    AnimatedPanel.prototype.ShowPanel = function () {
        var _this = this;
        var i = 0;
        this.FireAnimation(this.buttons, i, (function () { i++; _this.FireAnimation(_this.buttons, i, null); }));
    };
    AnimatedPanel.prototype.HidePanel = function () {
        var _this = this;
        var i = 0;
        this.FireAnimation(this.buttons, i, (function () { i++; _this.FireAnimation(_this.buttons, i, null); }));
    };
    AnimatedPanel.prototype.FireAnimation = function (buttons, i, func) {
        var _this = this;
        if (buttons[i] == null) {
            return;
        }
        if (func == null) {
            func = (function () { i++; _this.FireAnimation(buttons, i, null); });
        }
        Game._scene.beginAnimation(buttons[i], 0, 60, false, 3.0, func);
    };
    return AnimatedPanel;
}());
var ButtonFactory = /** @class */ (function () {
    function ButtonFactory() {
    }
    ButtonFactory.prototype.CreateImageButton = function (width, height) {
        var button = BABYLON.GUI.Button.CreateImageOnlyButton("button", "Decals.png");
        button.width = width.toString() + "px";
        button.height = height.toString() + "px";
        button.paddingLeft = 20;
        return button;
    };
    return ButtonFactory;
}());
