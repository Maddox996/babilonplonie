
class AnimatedPanel {

    private _buttonFactory : ButtonFactory;
    public stackPanel : BABYLON.GUI.StackPanel;
    public buttons : Array<any>;
    public isShown : boolean;
    
    constructor() {
        this.stackPanel = new BABYLON.GUI.StackPanel();
        this.stackPanel.isVertical = false;
        this._buttonFactory = new ButtonFactory(); // !
        this.isShown = false;
        this.buttons = new Array<any>();
        this.TestInit(); // !
    }
  
    public AddButton(height : number, width : number, callback : (eventData : BABYLON.GUI.Vector2WithInfo, eventState : BABYLON.EventState) => void) : BABYLON.GUI.Button {
        
        var control = new BABYLON.GUI.Container("container");
        control.width = width.toString() + "px";
        control.height = height.toString() + "px";

        var button = this._buttonFactory.CreateImageButton(0, 0); // !
        
        var animations = [];
        animations.push(Animations.GetWidthScaleAnimation(0, 60, width)); // !
        animations.push(Animations.GetHeightScaleAnimation(0, 60, height)); // !
        button['animations'] = animations; 
        
        control.addControl(button);
        this.stackPanel.addControl(control);
        button.onPointerDownObservable.add(callback, 1, true, this);
        this.buttons.push(button);
        return button;
    }

    public TestInit() : void {
 
        for(var i = 0; i < 5; i++) {
            var but = this.AddButton(150, 150, this.HidePanel);
        }
        
        this.ShowPanel();
    }

    Delay(milliseconds: number): Promise<void> {
        return new Promise<void>(resolve => {
                setTimeout(() => {           
                    resolve();
            }, milliseconds);
        });
    }

    public async ShowPanel() : Promise<void> {

        var panel = this; 
        for(var j = 0; j < panel.buttons.length; j ++) {
            await this.Delay(50).then(function(){
                panel.FireAnimations(panel.buttons, j, 0, 60);
            });
        }
    }

    public async HidePanel(a : BABYLON.GUI.Vector2WithInfo, b: BABYLON.EventState) : Promise<void> {

        var panel = this;
        for(var j = 0; j < panel.buttons.length; j ++) {
            await this.Delay(50).then(function(){
                panel.FireAnimations(panel.buttons, j, 60, 0);
            });
        }
    }
    
    public FireAnimations(buttons : Array<any>, i : number, startFrame : number, endFrame : number) : void {
        Game._scene.beginAnimation(buttons[i], startFrame, endFrame, false, 5.0);
    }
}



class ButtonFactory{
    public CreateImageButton(width: number, height: number) : BABYLON.GUI.Button {
        var button = BABYLON.GUI.Button.CreateImageOnlyButton("button", "Decals.png");
        button.width = width.toString() + "px";
        button.height = height.toString() + "px";
        button.paddingLeft = 20; //!!!!
        return button;
    }
}
  
  