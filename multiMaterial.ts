
class StickerController{

    static currentSticker : Sticker;

    camera : BABYLON.FreeCamera;
    renderTexture : BABYLON.RenderTargetTexture;
    scene : BABYLON.Scene;
    stickers : Sticker[];

    constructor(scene : BABYLON.Scene)
    {
        this.scene = scene;
        this.camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 55, -10),Game._scene, false);
        this.renderTexture = new BABYLON.RenderTargetTexture("render", 512, this.scene, true);
        this.renderTexture.coordinatesIndex = 0;
        this.renderTexture.activeCamera = this.camera;
        this.stickers = [];
        this.scene.customRenderTargets.push(this.renderTexture);
    }
    
    CreateSticker() : void
    {
        var sticker = new Sticker(this.scene);
        this.stickers.push(sticker);
        this.renderTexture.renderList.push(sticker.stickerMesh);
        StickerController.currentSticker = sticker;
    }

    CreateLatexableCube() : void
    {
        var cube2 = BABYLON.Mesh.CreateBox("x", 3, this.scene);
        var whiteTex = new BABYLON.Texture("/textures/white.jpg", this.scene);
        whiteTex.coordinatesIndex = 1;
        
        var material2 = new BABYLON.StandardMaterial("mat2", this.scene);
        material2.bumpTexture = this.renderTexture;
        material2.diffuseTexture = whiteTex;
        
        cube2.material = material2;
    }
}


class Sticker {

    scene : BABYLON.Scene;
    stickerMesh : BABYLON.Mesh;

    constructor(scene : BABYLON.Scene)
    {
        this.scene = scene;
        this.stickerMesh = BABYLON.Mesh.CreatePlane("plane", 5, this.scene);
        this.stickerMesh.position.y += 55.0;

        var material0 = new BABYLON.StandardMaterial("mat0", this.scene);
        material0.diffuseColor = new BABYLON.Color3(1, 0, 0);
        material0.bumpTexture = new BABYLON.Texture("/textures/normalMap.jpg", this.scene);
        this.stickerMesh.material = material0;

        var x = 0;

        this.scene.registerAfterRender(function(){

            x += 0.05;
            StickerController.currentSticker.stickerMesh.position.x = Math.sin(x);
        })

    }
}